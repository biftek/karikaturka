package us.mron.Karikaturka.adp;

import java.util.List;

import us.mron.Karikaturka.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CaricatureListAdapter extends ArrayAdapter<String> {

	private Context context;
	private List<String> items;
	public CaricatureListAdapter(Context context, int resource, List<String> items) {
		super(context, resource, items);
		this.context = context;
		this.items = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		 View rowView = convertView;
         if (rowView == null) {
             LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             rowView = vi.inflate(R.layout.caricature_list_item, null);
         }
         String caricatureImgUrl = items.get(position);
         if (items != null) {
        	 TextView txtCaricatureDate = (TextView) rowView.findViewById(R.id.txtCaricatureDate);
        	 ImageView imgCaricature = (ImageView) rowView.findViewById(R.id.imgCaricature);
			if (txtCaricatureDate != null) {
				txtCaricatureDate.setText(caricatureImgUrl);
			}
			if (imgCaricature != null) {
			}
         }
         return rowView;
	}

}
