package us.mron.Karikaturka;

import java.util.ArrayList;
import java.util.List;

import us.mron.Karikaturka.adp.CaricatureListAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class CaricatureList extends Activity {
	
	private ListView listvCaricature;
	private CaricatureListAdapter caricatureListAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caricature_list);
        
        init();
    }
    
    
    private void init() {
    	initVar();
    	initHand();
    	runFilling();
    }
    
    private void initVar() {
    	listvCaricature = (ListView)findViewById(R.id.listvCaricature);
    	
    	List<String> aaa = new ArrayList<String>();
    	aaa.add("Sinan");
    	aaa.add("Mehmet");
    	aaa.add("Batu");
    	
    	caricatureListAdapter = new CaricatureListAdapter(this, R.layout.caricature_list_item, aaa);
        
    }
    
    private void initHand() {
    	
    }
    
    private void runFilling() {
    	listvCaricature.setAdapter(caricatureListAdapter);
    	
    }

}
